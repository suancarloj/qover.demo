import React from 'react';
import plans from '../../images/plans.svg'
import styled from 'styled-components'
import { Colors } from '../../constants'
import Switch from '@material-ui/core/Switch';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useHistory, useLocation } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '6px',
      '& .MuiSwitch-track': {
        backgroundColor: Colors.white,
        borderRadius: '20px',
        opacity: 0.2
      },
      '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
        opacity: 0.2
      }
    }
  })
);

const SelectPlanScreen = () => {
  const classes = useStyles()
  const location = useLocation().state as any
  const history = useHistory()


  const [state, setState] = React.useState({
    payYearly: true,
  });

  if (!location || !location.priceGlobal || !location.priceUniversal) history.push('/quote')

  const priceGlobal = state.payYearly ? Math.round(location.priceGlobal * 100) / 100 : Math.round((location.priceGlobal / 12 + Number.EPSILON) * 100) / 100
  const priceUniversal = state.payYearly ? Math.round(location.priceUniversal * 100) / 100 : (Math.round((location.priceUniversal / 12 + Number.EPSILON) * 100) / 100)

  const handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [name]: event.target.checked });
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <PlansContainer>
        <Title>Select a plan</Title>
        <SwitchContainer>
          <SwitchText bold={!state.payYearly}>PAY MONTHLY</SwitchText>
          <Switch className={classes.root} color='default' checked={state.payYearly} onChange={handleChange('payYearly')} value="payYearly" inputProps={{ 'aria-label': 'primary checkbox' }} />
          <SwitchText bold={state.payYearly}>PAY YEARLY</SwitchText>
        </SwitchContainer>
        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
          <Plan backgroundColor={Colors.aquaMarine} textColor={Colors.white}>
            <PlanTitle>GLOBAL</PlanTitle>
            <Quote value={priceGlobal}>
            </Quote>
            <List>
              <ListItem>Maximum duration travel <Light>of</Light> 90 days</ListItem>
              <ListItem>Medical expenses reimbursement <Light>up to</Light> 1.000.000 €</ListItem>
              <ListItem>Personal assistance abroad <Light>up to</Light> 5.000 €</ListItem>
              <ListItem>Travel assistance abroad <Light>up to</Light> 1.000 <Light><br />per insured per travel</Light></ListItem>
              <ListItem>Coverage duration: 1 year</ListItem>
            </List>
          </Plan>
          <Plan backgroundColor={Colors.white} textColor={Colors.black}>
            <PlanTitle>UNIVERSE</PlanTitle>
            <Quote value={priceUniversal} />
            <List>
              <ListItem>Maximum duration travel <Light>of</Light> 180 days</ListItem>
              <ListItem>Medical expenses reimbursement <Light>up to</Light> 3.000.000 €</ListItem>
              <ListItem>Personal assistance abroad <Light>up to</Light> 10.000 €</ListItem>
              <ListItem>Travel assistance abroad <Light>up to</Light> 2.500 <Light><br />per insured per travel</Light></ListItem>
              <ListItem>Coverage duration: 1 year</ListItem>
            </List>
          </Plan>
        </div>
      </PlansContainer>
    </div>
  )
}


type QuoteProps = {
  value: number,
  children?: React.ReactNode
}

const Light = styled.span`
  font-weight: 300;
`

const Quote = (props: QuoteProps) => {
  const QuoteContainer = styled.div`
    background-color: rgba(255,255,255,0.1);
    padding: 15px;
    border-color: rgba(91, 114, 137, 0.1);
    border-width: 2px 0 2px 0;
    border-style: solid;
  `

  const QuoteNumber = styled.h1`
    font-size: 38px;
    margin: 0;
    margin-right: 5px;
    font-weight: bold;
  `

  const Currency = styled.span`
    font-size: 16px;
    margin-top: 5px;
  `

  const QuoteNumberWrapper = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: row;
  `

  const Taxes = styled.span`
    font-size: 11px;
  `


  return (
    <QuoteContainer>
      <QuoteNumberWrapper>
        <QuoteNumber>{props.value}</QuoteNumber>
        <Currency>€</Currency>
      </QuoteNumberWrapper>
      <Taxes>YEARLY INCL. TAXES</Taxes>
    </QuoteContainer>
  )


}
export default SelectPlanScreen

const PlansContainer = styled.div`
  background-image: url(${plans});
  background-repeat: no-repeat;
  background-position: top center;
  padding: 4rem;
  width: 935px;
`

type PlanProps = {
  backgroundColor: string
  textColor: string
}

const Plan = styled.div`
  background-color: ${(props: PlanProps) => props.backgroundColor};
  color: ${(props: PlanProps) => props.textColor};
  height: 470px;
  width: 323px;
  margin-right: 15px;
  border-radius: 3px;
  box-shadow: 0 2px 4px 0 rgba(72, 72, 72, 0.5);
  font-family: 'Roboto', sans-serif;
`

const List = styled.ul`
  list-style-type: none;
  padding: 0;
`
const ListItem = styled.li`
  padding: 15px;
  font-size: 12px;
  font-weight: 500;
  border-color: rgba(91, 114, 137, 0.1);
  border-width: 0 0 2px 0;
  border-style: solid;
`

const PlanTitle = styled.h3`
  font-size: 18px;
  font-weight: bold;
`

const SwitchContainer = styled.div`
  justify-content: center;
  margin: 47px;
`

type SwitchTextProps = {
  bold: boolean
}

const SwitchText = styled.span`
  color: ${Colors.white};
  font-size: 13px;
  letter-spacing: 1.08px;
  font-family: Roboto, sans-serif;
  font-weight: ${(props: SwitchTextProps) => props.bold ? 'bold' : '400'};
`
const Title = styled.h1`
  font-family: museo-sans, sans-serif;
  font-weight: 700;
  color: ${Colors.white};
  font-size: 26px;
` 