import Sizes from './Sizes'
import Colors from './Colors'
export {
    Colors,
    Sizes
}