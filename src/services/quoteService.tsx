import axios from 'axios'

const apiUrl = 'http://localhost:4000/api'

const getQuote = async (age: string, car: number, price: number): Promise<any> => {
    const response = await axios.post(`${apiUrl}/quote`, { age, car, price })
    return response.data
}

export default {
    getQuote
}