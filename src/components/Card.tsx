import React, { FunctionComponent } from 'react'
import { Colors, Sizes } from '../constants'
import styled from 'styled-components'

type Props = {
    children: React.ReactNode
    size: Sizes
}

type RoundedBoxProps = { 
    size: Sizes
}

const Card: FunctionComponent<Props> = ({ children, size }) => (
    <RoundedBox size={size}>
        {children}
    </RoundedBox>
)

const RoundedBox = styled.div`
  background-color: ${Colors.white};
  padding: ${(props: RoundedBoxProps) => props.size === Sizes.small ? '2rem' : '5rem 2rem' };
  border-radius: 0.3rem;
  margin-top: 3rem;
  width: ${(props: RoundedBoxProps) => props.size === Sizes.small ? '35rem' : '70rem' };
  box-shadow: 0 2px 2px 0 ${Colors.cloudyBlue};
  flex-direction: column;
  margin-bottom: 1rem;
`;

export default Card